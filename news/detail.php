<?php include "../header.php"; ?>
<?php

	$news = getNewsDetail($_GET['id']);	
?>
<div class="section section-background background-polygons" id="news">
	<div class="container news">

		<h1><?php echo $news['headline']; ?></h1>

		<?php
			
			 echo $news['body'];
		?>		

		<a class="btn btn-primary" href="/news">&larr; Back</a>
	</div>

		
</div>

<?php include "../footer.php"; ?>