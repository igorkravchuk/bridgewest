<?php include "../header.php"; ?>

<div class="section section-background background-polygons" id="news">

<div class="container">
		<h2 class="title-section">
			<span>News</span>
		</h2>

		<div class="news-items-container">
			<?php
				$releases = getNews();

				foreach($releases as $release) {
					$time = strtotime($release->modifiedDate);
			?>
			
			<div class="news-item">
				<p class="news-item-time">
					<?php echo date('m.d.Y', $time); ?>
				</p>

				<h4 class="news-item-title">
					<a href="/news/<?php echo $release->id; ?>">
						<?php echo $release->headline; ?>
					</a>
				</h4>

			</div>

			<?php
				}
			?>
		</div>
		<a class="btn btn-primary" href="/">&larr; Back</a>

	</div>
</div>

<?php include "../footer.php"; ?>