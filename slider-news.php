<div class="section section-background background-polygons">

	<div class="container">
		<h2 class="title-section">
			<span>News</span>
		</h2>

		<div id="carousel-news" class="carousel carousel-news slide" data-ride="carousel">

			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#carousel-news" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-news" data-slide-to="1"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">

				<div class="item active">

					<div class="row">

						<div class="col-sm-3">

							<div class="news-item">
								<p class="news-item-time">02.12.2015</p>
								<h4 class="news-item-title">Molecular Response Announces Sale of Business Unit to Crown Bioscience</h4>
								<p>San Diego, CA - Molecular Response (MRL), a privately held molecular diagnostic services company and a leader in the identification of target populations for high value therapeutics, announced today sale of its PDX business unit to Crown Bioscience, Inc.</p>
								<p><a href="#" class="news-item-readmore">Read more</a></p>								
							</div>

						</div>

						<div class="col-sm-3">

							<div class="news-item">
								<p class="news-item-time">02.12.2015</p>
								<h4 class="news-item-title">Molecular Response Announces Sale of Business Unit to Crown Bioscience</h4>
								<p>San Diego, CA - Molecular Response (MRL), a privately held molecular diagnostic services company and a leader in the identification of target populations for high value therapeutics, announced today sale of its PDX business unit to Crown Bioscience, Inc.</p>
								<p><a href="#" class="news-item-readmore">Read more</a></p>								
							</div>
							
						</div>

						<div class="col-sm-3">

							<div class="news-item">
								<p class="news-item-time">11.11.2014</p>
								<h4 class="news-item-title">BioAtla Partners with BioDuro to Advance Two Proprietary Antibody Therapeutics in China</h4>
								<p>San Diego, Calif. - November 11, 2014 - BioAtla, a global biotechnology company focused on the development of differentiated biological therapeutics, today announced an agreement with BioDuro to collaborate on the development of two therapeutic monoclonal antibodies in China. The two novel antibodies target specific indications with combined addressable populations of millions of patients.</p>
								<p><a href="#" class="news-item-readmore">Read more</a></p>								
							</div>
							
						</div>

						<div class="col-sm-3">

							<div class="news-item">
								<p class="news-item-time">02.12.2015</p>
								<h4 class="news-item-title">Molecular Response Announces Sale of Business Unit to Crown Bioscience</h4>
								<p>San Diego, CA - Molecular Response (MRL), a privately held molecular diagnostic services company and a leader in the identification of target populations for high value therapeutics, announced today sale of its PDX business unit to Crown Bioscience, Inc.</p>
								<p><a href="#" class="news-item-readmore">Read more</a></p>								
							</div>
							
						</div>

					</div>

				</div>


				<div class="item">

					<div class="row">
						

						<div class="col-sm-3">

							<div class="news-item">
								<p class="news-item-time">11.11.2014</p>
								<h4 class="news-item-title">BioAtla Partners with BioDuro to Advance Two Proprietary Antibody Therapeutics in China</h4>
								<p>San Diego, Calif. - November 11, 2014 - BioAtla, a global biotechnology company focused on the development of differentiated biological therapeutics, today announced an agreement with BioDuro to collaborate on the development of two therapeutic monoclonal antibodies in China. The two novel antibodies target specific indications with combined addressable populations of millions of patients.</p>
								<p><a href="#" class="news-item-readmore">Read more</a></p>								
							</div>
							
						</div>

						<div class="col-sm-3">

							<div class="news-item">
								<p class="news-item-time">02.12.2015</p>
								<h4 class="news-item-title">Molecular Response Announces Sale of Business Unit to Crown Bioscience</h4>
								<p>San Diego, CA - Molecular Response (MRL), a privately held molecular diagnostic services company and a leader in the identification of target populations for high value therapeutics, announced today sale of its PDX business unit to Crown Bioscience, Inc.</p>
								<p><a href="#" class="news-item-readmore">Read more</a></p>								
							</div>

						</div>

						<div class="col-sm-3">

							<div class="news-item">
								<p class="news-item-time">02.12.2015</p>
								<h4 class="news-item-title">Molecular Response Announces Sale of Business Unit to Crown Bioscience</h4>
								<p>San Diego, CA - Molecular Response (MRL), a privately held molecular diagnostic services company and a leader in the identification of target populations for high value therapeutics, announced today sale of its PDX business unit to Crown Bioscience, Inc.</p>
								<p><a href="#" class="news-item-readmore">Read more</a></p>								
							</div>
							
						</div>

						<div class="col-sm-3">

							<div class="news-item">
								<p class="news-item-time">02.12.2015</p>
								<h4 class="news-item-title">Molecular Response Announces Sale of Business Unit to Crown Bioscience</h4>
								<p>San Diego, CA - Molecular Response (MRL), a privately held molecular diagnostic services company and a leader in the identification of target populations for high value therapeutics, announced today sale of its PDX business unit to Crown Bioscience, Inc.</p>
								<p><a href="#" class="news-item-readmore">Read more</a></p>								
							</div>
							
						</div>

					</div>

				</div>

			</div>
		</div>

	</div>

</div>