
	<div class="container" id="home">

		<div id="carousel-home" class="carousel carousel-home slide" data-ride="carousel">

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">

				<div class="item active">
					<div class="carousel-picture" style="background-image: url('/images/slide-1.png')"></div>					
				</div>

				<div class="item">
					<div class="carousel-picture" style="background-image: url('/images/slide-2.png')"></div>
				</div>

				<div class="item">
					<div class="carousel-picture" style="background-image: url('/images/slide-3.png')"></div>
				</div>
			</div>

			<div class="carousel-static">
				<h3>Value Investor</h3>
				<p>The Bridgewest Group creates long-term value through the application of superior industry knowledge, operational expertise and significant financial resources to attractive investment opportunities.</p>
				
				<h3>Full-Spectrum Investing</h3>
				<p>The Bridgewest Group is a closely held investment company with global assets in the biotech, wireless communications, infrastructure for "internet of things," semiconductor, real estate, and financial services industries.</p>
				
				<h3>Global Investment Platform</h3>
				<p>Having been in business for over twenty-five years and headquartered in San Diego, California, the Bridgewest Group has operations across the USA, Europe and Asia.</p>
			</div>

			
		</div>

	</div>
