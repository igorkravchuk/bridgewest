<div class="section section-background background-polygons" id="news">

	<div class="container">
		<h2 class="title-section">
			<span>News</span>
		</h2>

		<div class="row news-items-container">

			<?php
				$releases = getNews();
				for ($i = 0; $i < 4; $i++) {
					$release = $releases[$i];
					$time = strtotime($release->modifiedDate);
				?>			

					<div class="news-item col-sm-3">
						<p class="news-item-time">
							<?php echo date('m.d.Y', $time); ?>						
						</p>
						<h4 class="news-item-title">
							<a href="/news/<?php echo $release->id; ?>"><?php echo $release->headline; ?></a>
						</h4>
						<!-- <p>
							<?php echo $release->headline; ?>
						</p> -->
						<p><a href="/news/<?php echo $release->id; ?>" class="news-item-readmore">Read more</a></p>
					</div>				
					
			<?php
				}
			?>

		</div>

		<br>
		
		<p class="text-center">
			<a href="/news" class="btn btn-primary">See More</a>
		</p>

	</div>

</div>