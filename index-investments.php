<div class="section-lines">
	<div class="container">
		<div class="row">
			<div class="col-sm-2 col-md-offset-1 col-md-1">
				<a class="order-button active" data-filter="*" data-filter-target="#filter-investments">All</a>
			</div>
			<div class="col-sm-4 col-md-4">
				<a class="order-button" data-filter="financial" data-filter-target="#filter-investments">Real Estate & Financial Services</a>
			</div>
			<div class="col-sm-3 col-md-3">
				<a class="order-button" data-filter="technology" data-filter-target="#filter-investments">Internet & Technology</a>
			</div>
			<div class="col-sm-3 col-md-2">
				<a class="order-button" data-filter="life" data-filter-target="#filter-investments">Life Sciences</a>
			</div>
		</div>
	</div>
</div>
<div class="section">
<div class="container" id="filter-investments">

	<div class="row">

		<div class="col-md-4 col-sm-6 text-center" data-cat="technology">

			<a href="http://www.enduratechnologies.com" class="invest-item" target="_blank">

				<span class="invest-caption">Endura Technologies is a fabless semiconductor company providing the next generation of power management solutions. Endura provides optimum system level solution for the SoC architecture at advanced process nodes.</span>

				<img src="images/investments/endura.png" alt="Endura Technologies">

			</a>

		</div>

		<div class="col-md-4 col-sm-6 text-center" data-cat="life">

			<a href="http://www.bioatla.com" class="invest-item" target="_blank">

				<span class="invest-caption">BioAtla uses deep expertise in protein and antibody engineering and proprietary technologies to solve critical protein development challenges and produce novel, IP-protectable products for our partners and our own product portfolio.</span>

				<img src="images/investments/bioatla.png" alt="bio Atla">

			</a>

		</div>



		<div class="col-md-4 col-sm-6 text-center" data-cat="life">

			<a href="http://www.formexllc.com" class="invest-item" target="_blank">

				<span class="invest-caption">Formex LLC is a leading contract development and manufacturing organization focusing on oral and topical dosage forms. Formex specializes in bioavailability enhancement and controlled release technologies, such as hot melt extrusion and spray drying.</span>

				<img src="images/investments/formex.png" alt="Formex">

			</a>

		</div>



		<div class="col-md-4 col-sm-6 text-center" data-cat="life">

			<a href="http://www.bioinv.com" class="invest-item" target="_blank">  

				<span class="invest-caption">Biotech Investment Group provides growth equity capital to emerging healthcare services companies, which are primarily focused on early stage drug discovery services, preclinical services and clinical services as well as enabling technologies in all these areas.</span>                                  

				<img src="images/investments/big.png" alt="BIG">

			</a>

		</div>



		<div class="col-md-4 col-sm-6 text-center" data-cat="life">

			<a href="http://www.molecularresponse.com" class="invest-item" target="_blank">

				<span class="invest-caption">Molecular Response is passionate about targeted therapeutics for the right patient.  Molecular Response is committed to highest quality clinical standards and committed to results.</span>

				<img src="images/investments/molecularresponse.png" alt="Molecular Response">

			</a>

		</div>



		<div class="col-md-4 col-sm-6 text-center" data-cat="financial">

			<a href="http://www.bridgewestcapital.com" class="invest-item" target="_blank">

				<span class="invest-caption">Bridgewest Capital provides tailored financing solutions to small and middle market companies in all industries.  Bridgewest Capital is led by a team of accomplished investment and operating professionals with expertise in life sciences, technology and commercial real estate.</span>

				<img src="images/investments/bridgewestcapital.png" alt="Bridgewest Capital">

			</a>

		</div>



		<div class="col-md-4 col-sm-6 text-center" data-cat="technology">

			<a href="http://www.ubiqomm.com" class="invest-item" target="_blank">

				<span class="invest-caption">Ubiqomm develops wireless communications systems from initial feasibility study and detailed system design, to implementation and commercialization of the system. In addition, Ubiqomm provides a suite of fully managed services for such systems.</span>

				<img src="images/investments/ubiqomm.png" alt="Ubiqomm">

			</a>

		</div>



		<div class="col-md-4 col-sm-6 text-center" data-cat="technology">

			<a href="http://www.gimbal.com" class="invest-item" target="_blank">

				<span class="invest-caption">Developed as a part of Qualcomm Labs, Gimbal brings together the proven technology that retailers, venues, and developers rely on, with the market-driven development cycle and nimble decision-making process that today's rapidly evolving proximity ecosystem requires.</span>

				<img src="images/investments/gimbal.png" alt="Gimbal">

			</a>

		</div>



		<div class="col-md-4 col-sm-6 text-center" data-cat="life">

			<a href="http://www.patarapharma.com" class="invest-item" target="_blank">

				<span class="invest-caption">Patara Pharma is a biopharmaceutical company developing a new therapy for the treatment of allergic and immunologic diseases and conditions that affect orphan patient populations.</span>

				<img src="images/investments/patara.png" alt="PATARA | PHARMA">

			</a>

		</div>



		<div class="col-md-4 col-sm-6 text-center" data-cat="financial">

			<a class="invest-item invest-item_nosite">

				<span class="invest-caption">Alliance uses its strong cash position to buy commercial real estate that can be repositioned, redeveloped, and leased to create long-term value.</span>

				<img src="images/investments/alliance.png" alt="Alliance">

			</a>

		</div>	



		<div class="col-md-4 col-sm-6 text-center" data-cat="financial">

			<a class="invest-item invest-item_nosite">

				<span class="invest-caption">Alliance Realty Capital acquires under-performing mortgages directly from financial institutions at below market pricing.  After acquisition, Alliance maximizes recovery through resolution strategies ranging from a borrower friendly loan modification program to foreclosure and eventual sale of the collateral.</span>

				<img src="images/investments/arc.png" alt="ARC">

			</a>

		</div>



		<div class="col-md-4 col-sm-6 text-center" data-cat="financial">

			<a href="http://www.arps.com" class="invest-item" target="_blank">

				<span class="invest-caption">ARPS is a property management company that currently manages over 1,100 doors in nine core markets across the country.</span>

				<img src="images/investments/arps.png" alt="ARPS">

			</a>

		</div>	



		<div class="col-md-4 col-sm-6 text-center" data-cat="technology">

			<a href="http://www.cyrus-avl.com/" class="invest-item" target="_blank">

				<span class="invest-caption">Cyrus Wireless provides products and high quality services designed to protect and locate people and goods.</span>

				<img src="images/investments/cyrus.png" alt="Cyrus Wireless">

			</a>

		</div>



		<div class="col-md-4 col-sm-6 text-center" data-cat="life">

			<a class="invest-item invest-item_nosite">

				<span class="invest-caption">Monoclonal Antibody Company focused on delivering powerful new anti-cancer therapeutics targeting inflammatory cytokine mediators.</span>

				<img src="images/investments/femta.png" alt="FEMTA Pharmaceuticals">

			</a>

		</div>



		<div class="col-md-4 col-sm-6 text-center" data-cat="technology">

			<a href="http://www.mir3.com/" class="invest-item" target="_blank">

				<span class="invest-caption">MIR3 is the premier provider of Intelligent Notification and response software for any area needing reliable two-way notification for groups from one to many thousands.</span>

				<img src="images/investments/mir3.png" alt="MIR3">

			</a>

		</div>




		<div class="col-md-4 col-sm-6 text-center" data-cat="technology">

			<a href="http://www.skyriver.net" class="invest-item" target="_blank">

				<span class="invest-caption">Skyriver's next-generation fixed wireless internet solutions deliver fiber equivalent bandwidth up to GigE speeds, and are designed for maximum efficiency, scalability, agility and reliability to meet the most critical business needs of enterprises.</span>

				<img src="images/investments/skyriver.png" alt="SkyRiver">

			</a>

		</div>



	</div>

</div>
</div>