<div id="team">
	<!--
	<div class="section-lines">
		<div class="container">
			<div class="row" data-filter-control>
				<div class="col-sm-4 col-md-offset-1 col-md-2">
					<a class="order-button active" data-filter="*" data-filter-target="#filter-team">All</a>
				</div>
				<div class="col-sm-4 col-md-5">
					<a class="order-button" data-filter="bridgewest" data-filter-target="#filter-team">Bridgewest</a>
				</div>
				<div class="col-sm-4 col-md-3">
					<a class="order-button" data-filter="group2" data-filter-target="#filter-team">Group 2</a>
				</div>
			</div>
		</div>
	</div>
	-->

	<div class="section section-background background-polygons" id="filter-team">

		<div class="container">
			<h2 class="title-section">
				<span>Team</span>
			</h2>
		</div>
		

		<div class="container-fluid" style="position: relative;">
			<div class="container">
				<div id="carousel-team" class="carousel carousel-team" data-interval="false">			

				<!-- Indicators -->
					<ol class="carousel-indicators">
						<li class="active" data-target="#carousel-team" data-slide-to="0" class="active" data-cat="bridgewest">
							<span class="indicator-preview">
								<span class="indicator-title">
									Massih<br>Tayebi
								</span>
							</span>
						</li>
						<li data-target="#carousel-team" data-slide-to="1" data-cat="bridgewest">
							<span class="indicator-preview">
								<span class="indicator-title">
									Masood<br>Tayebi
								</span>
							</span>
						</li>
						<li data-target="#carousel-team" data-slide-to="2" data-cat="bridgewest">
							<span class="indicator-preview">
								<span class="indicator-title">
									Kevin M.<br>Russell
								</span>
							</span>
						</li>
						<li data-target="#carousel-team" data-slide-to="3" data-cat="bridgewest">
							<span class="indicator-preview">
								<span class="indicator-title">
									Saum<br>Vahdat
								</span>
							</span>
						</li>

						<!--
						<li data-target="#carousel-team" data-slide-to="4" data-cat="group2">
							<span class="indicator-preview">
								<span class="indicator-title">
									Cyrus<br>Mirsaidi
								</span>
							</span>
						</li>
						<li data-target="#carousel-team" data-slide-to="5" data-cat="group2">
							<span class="indicator-preview">
								<span class="indicator-title">
									Brett<br>Hall
								</span>
							</span>
						</li>
						-->

						<li data-target="#carousel-team" data-slide-to="4" data-cat="bridgewest">
							<span class="indicator-preview">
								<span class="indicator-title">
									Chandima Mendis
								</span>
							</span>
						</li>
						<li data-target="#carousel-team" data-slide-to="5" data-cat="bridgewest">
							<span class="indicator-preview">
								<span class="indicator-title">
									Aja<br>Harbert
								</span>
							</span>
						</li>
						<li data-target="#carousel-team" data-slide-to="6" data-cat="bridgewest">
							<span class="indicator-preview">
								<span class="indicator-title">
									Chris<br>Wendle
								</span>
							</span>
						</li>
						<li data-target="#carousel-team" data-slide-to="7" data-cat="bridgewest">
							<span class="indicator-preview">
								<span class="indicator-title">
									Tom<br>Giffin
								</span>
							</span>
						</li>
						<!-- <li data-target="#carousel-team" data-slide-to="8" data-cat="bridgewest">
							<span class="indicator-preview">
								<span class="indicator-title">
									Brett<br>Hall
								</span>
							</span>
						</li> -->
					</ol>

					<br><br>	

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">

						<div class="item active" data-cat="bridgewest">

							<div class="team-member">
								<div class="team-member-pic">
									<div class="team-member-pic-title">
										Massih<br>Tayebi
									</div>
								</div>
								
								<div class="team-member-content">
									<h2 class="team-member-title">Massih Tayebi, Ph.D.</h2>

									<p>Dr. Massih Tayebi is a Founding Partner and Chairman of the Bridgewest Group, a closely held investment company with significant assets under management. Along with his brother, Dr. Masood Tayebi, Dr. Tayebi manages a portfolio of global investments in hi-tech and wireless technology, biotech, commercial real estate, banking and capital markets. Headquartered in San Diego, CA, the Bridgewest Group has over 1000 employees and operations in North America, Europe and Asia.</p> 

									<p>Dr. Tayebi and his team have a proven track record for successfully growing more than a dozen companies from seed to multinational status. Through his visionary leadership, Dr. Tayebi has created thousands of jobs globally and disrupted and significantly improved productivity in multiple industries.</p> 

									<p>Dr. Tayebi has led efforts to privatize, deploy, and manage telecom infrastructure in over 60 countries, making him and his brother pioneers in bringing wireless connectivity to billions of people. Additionally, Dr. Tayebi introduced cutting edge technology and forged novel business processes in both new and mature industries. Having created multiple public and private companies, his efforts have directly impacted global economic growth by introducing novel management methodology and tools to complacent industries, disrupting associated commerce and creating substantial shareholder value.</p>

									<p>Dr. Tayebi actively participates in several philanthropic activities. Along with his wife Haleh, Dr. Tayebi funded the Massih &amp; Haleh Tayebi Annual Scholarship fund supporting the education of children in need in many of the premier schools around the world. He has generously donated to local and international cultural centers, by being the Principal Financier and major contributor to building the Persian Cultural Center of San Diego. Dr. Tayebi is especially thankful for the opportunity to champion several causes benefiting underprivileged and abused children around the world, including building homes for the underprivileged in Mexico, curtailing cyber-bullying, being a major contributor to building hospitals for orphaned and disabled kids in the Yazd region, supporting hospitals and funding dozens of heart surgeries for children in need, and supporting universal education through building schools and funding educational staffing.</p> 

									<p>In 2016 Dr. Tayebi was awarded the Ellis Island Medal of Honor, for his contributions to, and exemplifying the values of, the United States of America.</p>

									<p>Dr. Tayebi earned his M.S., Ph.D. and post-doctoral while in the United Kingdom.</p> 

								</div>
							</div>

						</div>


						<div class="item" data-cat="bridgewest">
							
							<div class="team-member">
								<div class="team-member-pic">
									<div class="team-member-pic-title">
										Masood<br>Tayebi
									</div>
								</div>
								
								<div class="team-member-content">
									<h2 class="team-member-title">Masood Tayebi, Ph.D.</h2>
									
									<p>Dr. Masood Tayebi is a Founding Partner and CEO of the Bridgewest Group, a closely held investment company with significant assets under management.  Along with his brother, Dr. Massih Tayebi, Dr. Tayebi manages a portfolio of investments in hi-tech and wireless technology, bio-tech and pharmaceuticals, commercial real estate, banking and capital markets. Dr. Tayebi and his team have a track record successfully growing more than a dozen companies from seed to multinational status. Through these companies, Dr. Tayebi has created thousands of jobs globally. Through his visionary leadership, Dr. Tayebi has created thousands of jobs globally and disrupted and significantly improved productivity in multiple industries.</p> 

									<p>Dr. Tayebi has led efforts to privatize, deploy, and manage telecom infrastructure in over 60 countries, making him and his brother pioneers in bringing wireless connectivity to billions of people. Additionally, Dr. Tayebi has been a leader throughout the biotech industry. He founded the Biotech Investment Group in 2006 providing growth equity capital to emerging and innovative companies in the biotech industry. Having created multiple public and private companies, his efforts have directly impacted global economy growth by introducing novel management methodology and tools to complacent industries, disrupting associated commerce and creating substantial shareholder value.</p> 

									<p>Dr. Tayebi brings a wealth of experience and critical insights into the service sector, gained through his success in the telecom, biotech, and real estate industries. Currently Dr. Tayebi and his brother are creating an incubator in Southern California to attract entrepreneurs driving innovation and success, which they believe will drive the creation of a new “Silicon Valley” in Southern California.</p> 

									<p>Dr. Tayebi actively participates in several philanthropic activities.  Along with his wife Surinder, Dr. Tayebi founded the Lotus Children Foundation. His charitable interests also includes supporting local first responders, education, housing, and helping the families of current and former military personnel. Dr. Tayebi enjoys advising and teaching entrepreneurs through one-on-one sessions as well as volunteering as a speaker at various conferences and events.</p>

									<p>Dr. Tayebi and his brother received the Ernst and Young Entrepreneur of the Year award, American Electronics Association High Technology Industry Recognition Award for telecommunications, the Venture Capital Success Story award by the San Diego Venture Group, and Visionary of the Year by the Coastline Foundation.</p> 

									<p>Dr. Tayebi earned his M.S. in Electronics Engineering and Ph.D. in mobile radio propagation while in the United Kingdom.</p>

								</div>
							</div>

						</div>


						<div class="item" data-cat="bridgewest">
							
							<div class="team-member">
								<div class="team-member-pic">
									<div class="team-member-pic-title">
										Kevin M.<br>Russell
									</div>
								</div>
								
								<div class="team-member-content">
									<h2 class="team-member-title">Kevin M. Russell</h2>
									Kevin M. Russell is a Partner and the Group Chief Legal Officer of the Bridgewest Group. Prior to joining Bridgewest, Mr. Russell was the General Counsel for Emirates International Investment Company LLC (EIIC), a multi-billion dollar global asset manager based in Abu Dhabi, United Arab Emirates. Prior to joining EIIC, Mr. Russell was a Vice President at Cantor Fitzgerald LP, acting as lead counsel for several real estate and asset management businesses. He was previously an attorney with Skadden, Arps, Slate, Meagher & Flom LLP and Weil, Gotshal & Manges LLP in both New York and London. Mr. Russell has also served as an arbitrator in complex commercial disputes conducted under the ICC rules.
								</div>
							</div>

						</div>


						<div class="item" data-cat="bridgewest">
							
							<div class="team-member">
								<div class="team-member-pic">
									<div class="team-member-pic-title">
										Saum<br>Vahdat
									</div>
								</div>
								
								<div class="team-member-content">
									<h2 class="team-member-title">Saum Vahdat</h2>
									Saum Vahdat is Managing Partner of Bridgewest Ventures (BV) and the Bridgewest Group's Director of Finance and Corporate Strategy. Mr. Vahdat leads investment strategy for Bridgewest Ventures while leading corporate strategy for BV’s investment holdings. In this role, Mr. Vahdat also provides leadership to Bridgewest Ventures’ portfolio companies. He is also responsible for leading the Group's finance and business development initiatives. Previously Mr. Vahdat worked with Johnson & Johnson's venture capital platform, assisting with commercialization strategy formation and market research. Prior to J&J, Mr. Vahdat led financial analysis and business development at a real estate investment firm. Mr. Vahdat graduated from Cornell University with an MBA focused on Finance and Strategy.								</div>
							</div>

						</div>

						<!--
						<div class="item" data-cat="group2">
							
							<div class="team-member">
								<div class="team-member-pic">
									<div class="team-member-pic-title">
										Cyrus<br>Mirsaidi
									</div>
								</div>
								
								<div class="team-member-content">
									<h2 class="team-member-title">Cyrus Mirsaidi</h2>
									Cyrus Mirsaidi is the Chief Executive Officer of BioDuro LLC and senior advisor to the Bridgewest Group on its biotech investments. Mr. Mirsaidi brings both practical and strategic senior management experience for young start-ups to fortune 100 companies, with proven track record in the industry. He held several senior level management positions in his ten-year career at Nichols Institute Diagnostics (a division of Quest Diagnostics. As general manager and executive director at Ontogen, a small molecule discovery company, he managed operations of a subsidiary focused on high throughput synthesis and purification of novel chemical compounds. Mr. Mirsaidi served as Vice President at AltheaDx, a spin-out from Althea Technologies, a San Diego biologics GMP manufacturing company, and most recently served as CEO and founder of Molecular Response, LLC, a CRO focused on translational oncology.
								</div>
							</div>

						</div>
						-->
						
						<!--
						<div class="item" data-cat="group2">
							
							<div class="team-member">
								<div class="team-member-pic">
									<div class="team-member-pic-title">
										Brett<br>Hall
									</div>
								</div>
								
								<div class="team-member-content">
									<h2 class="team-member-title">Brett Hall, Ph.D.</h2>
									Brett Hall is the Chief Executive Officer of Molecular Response LLC and senior advisor to the Bridgewest Group on its biotech investments.  Dr. Hall is a U.S. Air Force veteran and former investment banker.  He obtained his B.S. from the Ohio State University and Ph.D. from West Virginia University, studying how the tumor microenvironment affects tumor growth, metastasis and survival. He did his post-doctoral work at St. Jude Children’s Hospital and Nationwide Children’s Hospital before accepting a tenure track faculty position at the Ohio State University, Department of Pediatrics. Dr. Hall’s laboratory focused on tumor-stromal cell modeling in physiologically relevant human tumor microenvironments.  He also led the design, development and operation of a multi-million dollar small animal imaging core facility at Nationwide Children’s Hospital.  He transitioned to Janssen, LLC. where he led the European translational medicine group and patient-derived xenograft biomarker efforts in Beerse, Belgium.  Dr. Hall transitioned to the U.S. to lead the J&J Hematologic Oncology Disease Area Stronghold translational group. In this new role, he oversaw the translational efforts of 8 clinical assets and registered two oncology drugs.  A third drug, Daratumumab, recently received accelerated approval by the FDA. In 2014, Dr. Hall transitioned to MedImmune, the biologics division of AstraZeneca, where he led the Oncology Translational Medicine group and over 15 clinical programs. Dr. Hall has over 35 scientific publications, a patent for ibrutinib clinical combinations and has been recognized for direct translational support and contributing to successful acquisition/partnering of multiple oncology drugs including ibrutinib and Daratumumab.
								</div>
							</div>

						</div>
						-->


						<div class="item" data-cat="bridgewest">
							
							<div class="team-member">
								<div class="team-member-pic">
									<div class="team-member-pic-title">
										Chandima Mendis
									</div>
								</div>
								
								<div class="team-member-content">
									<h2 class="team-member-title">Chandima Mendis</h2>
									Chandima Mendis is the Bridgewest Group's senior economist and portfolio manager focusing on bonds, commodities and currency. Mr. Mendis advises the Group with respect to its investments in emerging markets and real assets. He has been ranked among the leading emerging market debt fund managers and previously worked for several international asset managers, including Fortis. Mr. Mendis also worked as an economist at the International Monetary Fund in Washington DC. He graduated with a Doctorate in Economics from the University of Oxford.
								</div>
							</div>

						</div>
						
						<!-- <div class="item" data-cat="bridgewest">
							
							<div class="team-member">
								<div class="team-member-pic">
									<div class="team-member-pic-title">
										Cyrus<br>Mojdehi
									</div>
								</div>
								
								<div class="team-member-content">
									<h2 class="team-member-title">Cyrus Mojdehi</h2>
									Cyrus Mojdehi is the Managing Director of Alliance Real Estate Investments. Mr. Mojdehi manages a multi-million dollar portfolio of commercial and residential real estate, primarily consisting of value add assets held for cash flow as well as a secondary effort focused on repositioning and selling. He oversees the sourcing, acquisition, renovation, and disposition of the portfolio’s assets. Mr. Mojdehi is a graduate of Brown University.
								</div>
							</div>

						</div> -->
						
						<div class="item" data-cat="bridgewest">
							
							<div class="team-member">
								<div class="team-member-pic">
									<div class="team-member-pic-title">
										Aja<br>Harbert
									</div>
								</div>
								
								<div class="team-member-content">
									<h2 class="team-member-title">Aja Harbert</h2>
									Aja Harbert is the Director of Human Resources for the Bridgewest Group.  Ms. Harbert manages a fully integrated platform of human resource functions including life cycle recruitment, training and development, incentive compensation plan administration and performance management. Previously, Ms. Harbert worked as a Recruiting Manager and Business Development Specialist for government contract agencies on behalf of the State of Hawai’i and California. Ms. Harbert graduated with honors from Hawai’i Pacific University with a Bachelor of Arts degree in International Relations and a minor in Spanish.
								</div>
							</div>

						</div>

						<div class="item" data-cat="bridgewest">
							
							<div class="team-member">
								<div class="team-member-pic">
									<div class="team-member-pic-title">
										Chris<br>Wendle
									</div>
								</div>
								
								<div class="team-member-content">
									<h2 class="team-member-title">Chris Wendle</h2>
									Chris Wendle is the Controller for the Bridgewest Group, where his responsibilities include management of the accounting, insurance, and corporate compliance for the Group along with coordinating operations. Prior to this role, Mr. Wendle served as the Director of Operations and Senior Accountant for Bridgewest. His experience includes strategic leadership, operational analysis, corporate governance, and group communications. He received his Bachelor and Master’s degrees from the University of California, Santa Barbara, studying Business Economics, Accounting, and Education, graduating with distinction in leadership.
								</div>
							</div>

						</div>

						<div class="item" data-cat="bridgewest">
							
							<div class="team-member">
								<div class="team-member-pic">
									<div class="team-member-pic-title">
										Tom<br>Giffin
									</div>
								</div>
								
								<div class="team-member-content">
									<h2 class="team-member-title">Tom Giffin</h2>
									Tom Giffin is the Managing Director of Bridgewest Finance and President of Alliance Real Estate Holdings.  Bridgewest Finance is an asset-based lender with a California Finance Lenders license specializing in life science, technology and commercial real estate.  Alliance Real Estate Holdings owns a 400+ single family rental portfolio operating in 7 markets, manages residential rental assets for third parties and acquires homes for quick value-add resale.  Previously, Mr. Giffin worked at the FDIC as a Receiver in Charge, where he managed over 13 financial institutions with over $9 billion in assets.  Prior to the FDIC, Mr. Giffin worked as a Chief Financial Officer for real estate development companies and at Ocwen Financial Corporation acquiring and monetizing non-performing assets.  Mr. Giffin is a California licensed Certified Public Accountant and graduated from the University of California Santa Barbara with a BA in Business/Economics and Accounting.
 								</div>
							</div>

						</div>

						<!-- <div class="item" data-cat="bridgewest">
							
							<div class="team-member">
								<div class="team-member-pic">
									<div class="team-member-pic-title">
										Brett<br>Hall
									</div>
								</div>
								
								<div class="team-member-content">
									<h2 class="team-member-title">Brett Hall</h2>

 								</div>
							</div>

						</div> -->
						

					</div>

							
					
				</div>
			</div>

		</div>

	</div>
</div>