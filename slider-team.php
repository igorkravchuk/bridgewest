<div class="section section-background background-polygons">

	<div class="container">
		<h2 class="title-section">
			<span>Team</span>
		</h2>
	</div>




	<div class="container-fluid" style="position: relative;">
		<div class="container">
			<div id="carousel-team" class="carousel carousel-team slide" data-ride="carousel">

				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#carousel-team" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-team" data-slide-to="1"></li>
					<li data-target="#carousel-team" data-slide-to="2"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">

					<div class="item active">

						<div class="team-member team-member-left">
							<div class="team-member-title">
								<div class="team-member-title-cell">
									<h2>Masood Tayebi, Ph.D.</h2>
									<div>Partner and Chief Executive Officer</div>
								</div>
							</div>

							<div class="team-member-content">
								<p>
									Dr. Masood Tayebi is a Partner and Chief Executive Officer of the Bridgewest Group. He currently serves as CEO of a nationwide real estate portfolio and is the Founder and Chairman of Biotech Investment Group (BIG). Dr. Tayebi was the Founder and CEO of BioDuro, a contract research organization, which was successfully exited in 2012 through a sale to Pharmaceutical Product Development LLC. Prior to BioDuro, Dr. Tayebi was Co-Founder and Chairman of Wireless Facilities, Inc. (NASDAQ: WFI), a global leader in telecommunications outsourcing.
								</p>
							</div>
						</div>

					</div>


					<div class="item">	

						<div class="team-member team-member-right">
							<div class="team-member-title">
								<div class="team-member-title-cell">
									<h2>Masood Tayebi, Ph.D.</h2>
									<div>Partner and Chief Executive Officer</div>
								</div>
							</div>

							<div class="team-member-content">
								<p>
									Dr. Masood Tayebi is a Partner and Chief Executive Officer of the Bridgewest Group. He currently serves as CEO of a nationwide real estate portfolio and is the Founder and Chairman of Biotech Investment Group (BIG). Dr. Tayebi was the Founder and CEO of BioDuro, a contract research organization, which was successfully exited in 2012 through a sale to Pharmaceutical Product Development LLC. Prior to BioDuro, Dr. Tayebi was Co-Founder and Chairman of Wireless Facilities, Inc. (NASDAQ: WFI), a global leader in telecommunications outsourcing.
								</p>
							</div>
						</div>

					</div>


					<div class="item">
						
						<div class="team-member team-member-left">
							<div class="team-member-title">
								<div class="team-member-title-cell">
									<h2>Masood Tayebi, Ph.D.</h2>
									<div>Partner and Chief Executive Officer</div>
								</div>
							</div>

							<div class="team-member-content">
								<p>
									Dr. Masood Tayebi is a Partner and Chief Executive Officer of the Bridgewest Group. He currently serves as CEO of a nationwide real estate portfolio and is the Founder and Chairman of Biotech Investment Group (BIG). Dr. Tayebi was the Founder and CEO of BioDuro, a contract research organization, which was successfully exited in 2012 through a sale to Pharmaceutical Product Development LLC. Prior to BioDuro, Dr. Tayebi was Co-Founder and Chairman of Wireless Facilities, Inc. (NASDAQ: WFI), a global leader in telecommunications outsourcing.
								</p>
							</div>
						</div>

					</div>				

				</div>
			</div>
		</div>

		<!-- Controls -->
		<a class="left carousel-control carousel-control-simple" href="#carousel-team" role="button" data-slide="prev">
			<span class="icon-prev" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>

		<a class="right carousel-control carousel-control-simple" href="#carousel-team" role="button" data-slide="next">
			<span class="icon-next" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>


	</div>

</div>