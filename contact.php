<div class="section section-background background-polygons" id="contact">
	<div class="container">
		<h2 class="title-section">
			<span>Contact</span>
		</h2>
		<br>

		<div class="row">
			<div class="col-sm-6">
				<p>
					The Bridgewest Group is actively seeking investment opportunities and is interested in engaging with entrepreneurs regarding their ideas and business proposals. Our value proposition is built on a foundation of significant investible capital, tested management expertise and an extensive network of tier 1 investors that are eager to partner with us to build businesses and create long-term shareholder value.				
				</p>
				<p>
					While we we are generally agnostic as to asset class and industry, the Bridgewest Group has significant expertise in the biotechnology, material technology, financial services and the real estate sectors.
				</p>
			</div>
			<div class="col-sm-6">
				<form action="/mail.php">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								<input class="form-control" type="text" placeholder="Name *" name="name" required>
							</div>
							<div class="col-sm-6">
								<input class="form-control" type="text" placeholder="Phone *" name="phone" required>
							</div>
						</div>
					</div>

					<div class="form-group">
						<input class="form-control" type="text" placeholder="Email *" name="email" required>
					</div>
					<div class="form-group">
						<textarea class="form-control" rows="5" placeholder="Message *" name="message" required></textarea>
					</div>

					<div class="alert alert-success form-success" style="display: none">Your idea has been sent successfully</div>
					<div class="alert alert-danger form-error" style="display: none">Error, something went wrong. Try again later</div>

					<div class="form-group text-right">
						<button type="submit" class="btn btn-lg btn-primary" style="padding-left: 40px; padding-right: 40px;">Submit</button>
					</div>

				</form>

			</div>
		</div>
	</div>
</div>

<div class="section-map">
	<div class="container">
		<img src="/images/map.png" class="center-block img-responsive">
	</div>
</div>