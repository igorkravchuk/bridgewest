<?php include 'core.php' ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Bridgewest Group</title>

	<link rel="icon" href="/favicon.ico"> 

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

	<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="/styles/main.css">


</head>
<body data-spy="scroll" data-target="#navbar-collapse" data-offset="90">

	<nav class="navbar navbar-custom navbar-fixed-top">
		<div class="container">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<a href="/" class="navbar-brand">
					<img src="/images/logo.png" alt="">
				</a>
			</div>

			<div id="navbar-collapse" class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">

					<li data-toggle="collapse" data-target="#navbar-collapse.in">
						<a href="#home" class="page-scroll">Home</a>
					</li>

					<li data-toggle="collapse" data-target="#navbar-collapse.in">
						<a href="#team" class="page-scroll">Team</a>
					</li>

					<li data-toggle="collapse" data-target="#navbar-collapse.in">
						<a href="#investments" class="page-scroll">Select Investments</a>
					</li>
					
					<li data-toggle="collapse" data-target="#navbar-collapse.in">
						<a href="#news" class="page-scroll">News</a>
					</li>

					<li data-toggle="collapse" data-target="#navbar-collapse.in">
						<a href="#citizenship" class="page-scroll">Citizenship</a>
					</li>

					<li data-toggle="collapse" data-target="#navbar-collapse.in">
						<a href="#contact" class="page-scroll">Contact</a>
					</li>

			</div>

		</div>
	</nav>