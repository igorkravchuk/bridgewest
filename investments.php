<div class="section-lines" id="investments">
	<div class="container">
		<div class="row" data-filter-control>
			<div class="col-sm-3 col-md-offset-1 col-md-2">
				<a class="order-button active" data-filter="*" data-filter-target="#filter-investments">All</a>
			</div>
			<div class="col-sm-3 col-md-3">
				<a class="order-button" data-filter="financial" data-filter-target="#filter-investments">Fixed Income</a>
			</div>
			<div class="col-sm-3 col-md-2">
				<a class="order-button" data-filter="technology" data-filter-target="#filter-investments">Real Estate</a>
			</div>
			<div class="col-sm-3 col-md-3">
				<a class="order-button" data-filter="life" data-filter-target="#filter-investments">Private Equity</a>
			</div>
		</div>
	</div>
</div>
<div class="section">
	<div class="container" id="filter-investments">

		<div class="row">

			<div class="col-md-4 col-sm-6 text-center" data-cat="life">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="http://www.enduratechnologies.com" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/endura.png" alt="Endura Technologies">

					<span class="invest-caption">Endura Technologies is a fabless semiconductor company providing the next generation of power management solutions. Endura provides optimum system level solution for the SoC architecture at advanced process nodes.
					<br><br>
					* a Bridgewest Ventures company
					</span>
				</div>

			</div>		

			<div class="col-md-4 col-sm-6 text-center" data-cat="life">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="http://www.wavious.com" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/wavious.png" alt="Wavious">

					<span class="invest-caption">Creative and innovative platform based solution addressing economics of innovation that has adversely affected the semiconductor industry growth while maintaining highest level of system efficiency.  Significant system level  NRE , production cost and  BoM reduction.  Software configurable platform  to address  vertical  and adjacent markets with optimum performance and power at lowest cost structure. Enablement of emerging markets at better than present  SoC  power, performance and production cost; Ideal for platform development (IoT, MoT, Automotive, etc.)
					<br><br>
					* a Bridgewest Ventures company
					</span>

				</div>

			</div>

			<div class="col-md-4 col-sm-6 text-center" data-cat="technology">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<img src="images/investments/alliance.png" alt="Alliance">

					<span class="invest-caption">Alliance Business Holdings is a diversified real estate investor and operator with commercial and residential holdings throughout the United States including California, Ohio, Tennessee, North Carolina, Illinois, Pennsylvania and New York.  The company focuses on long term value creation and positive cash flow, and is professionally directed by a team that understands dynamic markets, appreciates risks and tailors sophisticated rental strategies to the unique characteristics of the asset.  Alliance Business Holdings targets investment opportunities where long term value and cash flow can be maximized through value add strategies combined with strong financial and operational management.</span>			

				</div>

			</div>

			<div class="col-md-4 col-sm-6 text-center" data-cat="technology">

				<div href="" class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="http://www.arps.com" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/arps.png" alt="ARPS">

					<span class="invest-caption">ARPS is a property management company that currently manages over 1,100 doors in nine core markets across the country.</span>				

				</div>

			</div>	

			<div class="col-md-4 col-sm-6 text-center" data-cat="life">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="http://www.bioatla.com" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/bioatla.png" alt="bio Atla">

					<span class="invest-caption">BioAtla uses deep expertise in protein and antibody engineering and proprietary technologies to solve critical protein development challenges and produce novel, IP-protectable products for our partners and our own product portfolio.</span>

				</div>

			</div>

			<div class="col-md-4 col-sm-6 text-center" data-cat="life">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="http://bioduro.com" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/bioduro.png" alt="BioDuro">

					<span class="invest-caption">Founded in 2005, BioDuro is a fully integrated drug discovery and development service company. From drug substance to drug product, discovery to development, small molecule or biologics, BioDuro’s solutions provide pharmaceutical and biopharmaceutical companies with the quality tools they need to deliver their drug to market.
					<br><br>
					* a Bridgewest Ventures company
					</span>
				</div>

			</div>

			<div class="col-md-4 col-sm-6 text-center" data-cat="life">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="http://f1oncology.com" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/f1.png" alt="F1 Oncology">

					<span class="invest-caption">F1 Oncology leverages its globally integrated science, development and informatics teams to accelerate the design, high-throughput screening, discovery and development of adoptive cellular therapy candidates.</span>				

				</div>

			</div>

			<div class="col-md-4 col-sm-6 text-center" data-cat="life">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="//www.bioardis.com" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/bioardis.png" alt="Bioardis">

					<span class="invest-caption">BioArdis is a pre-clinical investment and biotechnology company focused on value creation through efficient small molecule drug discovery; perfecting the process from Lead through Preclinical Development Candidate (PDC).
					<br><br>
					* a Bridgewest Ventures company
					</span>				

				</div>

			</div>

			<div class="col-md-4 col-sm-6 text-center" data-cat="financial">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<img src="images/investments/bridgewestcapital.png" alt="Bridgewest Capital">

					<span class="invest-caption">Bridgewest Capital LLC is an investment manager that provides superior risk adjusted returns to its investors. Bridgewest Capital focuses on fixed income markets and trading strategies of global and emerging market bonds, credit, currencies and commodities. The portfolios are managed using a fundamentally driven top-down investment approach for sovereign bond management combined with a rigorous process for managing liquidity and risk.</span>				

				</div>

			</div>

			<div class="col-md-4 col-sm-6 text-center" data-cat="financial">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<!-- <a href="http://www.bridgewestfinance.com" target="_blank" class="invest-link">Learn more</a> -->

					<img src="images/investments/bridgewestfinance.png" alt="Bridgewest Finance">

					<span class="invest-caption">Bridgewest Finance provides tailored financing solutions to small and middle market companies in all industries. Bridgewest Finance is led by a team of accomplished investment and operating professionals with expertise in life sciences, technology and commercial real estate.</span>				

				</div>

			</div>

			<div class="col-md-4 col-sm-6 text-center" data-cat="financial">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="http://www.bridgewestfinance.nz" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/bridgewestfinancenz.png" alt="Bridgewest Finance New Zealand">

					<span class="invest-caption">Bridgewest Finance New Zealand is a leading specialty finance firm that provides financing solutions to small and medium sized companies. The Bridgewest Group, with over $1 billion under management globally, has significant experience lending and investing across multiple industries including life sciences, technology, and commercial real estate.</span>				

				</div>

			</div>

			<!-- <div class="col-md-4 col-sm-6 text-center" data-cat="life">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="http://www.patarapharma.com" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/patara.png" alt="PATARA | PHARMA">

					<span class="invest-caption">Patara Pharma is a biopharmaceutical company developing a new therapy for the treatment of allergic and immunologic diseases and conditions that affect orphan patient populations.</span>				

				</div>

			</div> -->

			<!-- <div class="col-md-4 col-sm-6 text-center" data-cat="technology">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="http://www.cyrus-avl.com/" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/cyrus.png" alt="Cyrus Wireless">

					<span class="invest-caption">Cyrus Wireless provides products and high quality services designed to protect and locate people and goods.</span>				

				</div>

			</div> -->
			
			<!-- <div class="col-md-4 col-sm-6 text-center" data-cat="life">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="http://asellustherapeutics.com" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/asellus-logo.png" alt="Asellus Therapeutics">
					<span class="invest-caption">Asellus Therapeutics utilizes intelligently-designed tumor models to discover and develop novel oncology drugs.
					<br><br>
					* a Bridgewest Ventures company
					</span>
				</div>

			</div> -->

			<!-- <div class="col-md-4 col-sm-6 text-center" data-cat="technology">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="http://www.mir3.com/" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/mir3.png" alt="MIR3">

					<span class="invest-caption">MIR3 is the premier provider of Intelligent Notification and response software for any area needing reliable two-way notification for groups from one to many thousands.</span>				

				</div>

			</div> -->

			<!--<div class="col-md-4 col-sm-6 text-center" data-cat="technology">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="http://www.gimbal.com" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/gimbal.png" alt="Gimbal">

					<span class="invest-caption">Developed as a part of Qualcomm Labs, Gimbal brings together the proven technology that retailers, venues, and developers rely on, with the market-driven development cycle and nimble decision-making process that today's rapidly evolving proximity ecosystem requires.</span>				

				</div>

			</div>-->
			
			<!--<div class="col-md-4 col-sm-6 text-center" data-cat="financial">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<img src="images/investments/arc.png" alt="ARC">

					<span class="invest-caption">Alliance Realty Capital acquires under-performing mortgages directly from financial institutions at below market pricing.  After acquisition, Alliance maximizes recovery through resolution strategies ranging from a borrower friendly loan modification program to foreclosure and eventual sale of the collateral.</span>				

				</div>

			</div>-->

			<!-- <div class="col-md-4 col-sm-6 text-center" data-cat="life">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="http://www.bioinv.com" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/big.png" alt="BIG">

					<span class="invest-caption">Biotech Investment Group provides growth equity capital to emerging healthcare services companies, which are primarily focused on early stage drug discovery services, preclinical services and clinical services as well as enabling technologies in all these areas.</span>				

				</div>

			</div> -->

			<!-- <div class="col-md-4 col-sm-6 text-center" data-cat="life">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="http://www.molecularresponse.com" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/molecularresponse.png" alt="Molecular Response">

					<span class="invest-caption">Molecular Response is passionate about targeted therapeutics for the right patient.  Molecular Response is committed to highest quality clinical standards and committed to results.</span>				

				</div>

			</div> -->
			
			<!-- <div class="col-md-4 col-sm-6 text-center" data-cat="technology">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="http://www.skyriver.net" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/skyriver.png" alt="SkyRiver">

					<span class="invest-caption">Skyriver's next-generation fixed wireless internet solutions deliver fiber equivalent bandwidth up to GigE speeds, and are designed for maximum efficiency, scalability, agility and reliability to meet the most critical business needs of enterprises.</span>				

				</div>

			</div> -->

			<!-- <div class="col-md-4 col-sm-6 text-center" data-cat="technology">

				<div class="invest-item">
					
					<span class="invest-close">×</span>

					<a href="http://www.ubiqomm.com" target="_blank" class="invest-link">Learn more</a>

					<img src="images/investments/ubiqomm.png" alt="Ubiqomm">

					<span class="invest-caption">Ubiqomm develops wireless communications systems from initial feasibility study and detailed system design, to implementation and commercialization of the system. In addition, Ubiqomm provides a suite of fully managed services for such systems.
					<br><br>
					* a Bridgewest Ventures company
					</span>
				</div>

			</div> -->

		</div>

	</div>
</div>