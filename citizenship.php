<div class="section" id="citizenship">
	<div class="container">
		<h2 class="title-section">
			<span>Citizenship</span>
		</h2>

		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h2 class="text-center">Community Responsibility</h2>
				<p>
					The Bridgewest Group is genuinely proud to call San Diego, CA home. Service to the San Diego community is a cornerstone of the Bridgewest Group’s mission and commitment as a local business leader. We believe it is our responsibility to give back to America’s Finest City, a city that has contributed greatly to our firm’s success over the past two decades.  The Bridgewest Group actively invests in San Diego and global communities through diverse contributions to charities that have a strong track record of generating superior social return on investment.
				</p>
				<h2 class="text-center">Our Founders’ Philanthropy</h2>
				<p>
					A steadfast commitment to the city of San Diego is a philosophy driven by our Founders, Dr. Massih Tayebi and Dr. Masood Tayebi. Dr. Massih Tayebi and Dr. Masood Tayebi, along with their respective families, are actively involved in positively contributing to the San Diego community. Their dedication to San Diego is manifested through their championing a wide range of causes, including organizations benefiting underprivileged children, military families and entrepreneurship.
				</p>
				<p>
					In addition to their enduring commitment to the city of San Diego, the Tayebi family is proud to have partnered with numerous organizations that reflect their vision for a more unified and peaceful world. 
				</p>
				<p>
					Below is a selection of local and international charitable organizations that the Tayebis are honored to support. 
				</p>

				
			</div>
		</div>

		<br><br><br><br>
		

		<div class="row">

			<div class="col-sm-4">
				
				<img src="images/logo-family.png" alt="" class="center-block">

				<br>

				<p>To support individual and family readiness through an array of programs specifically targeted to assist the Naval Special Warfare community in maintaining a resilient, sustainable, and healthy force in this era of persistent conflict and frequent deployments.</p>

				<br>

				<p>Their vision is to be the premier provider of benevolent civilian support services to the NSW communities they serve. This is pursued by collaborating with the leaders in Family Support to better understand the effects of deployment; by frequent analysis to ensure they are meeting current needs; by the delivery of exceptional programs; and by educating and involving the civilian population in support of these elite warriors.</p>

			</div>

			<div class="col-sm-4">

				<img src="images/logo-rosie.png" class="center-block">

				<br>

				<p>The San Diego Military Family Collaborative’s mission provides an inclusive forum to maximize the collective impact of community resources to enhance military family life.</p>

			</div>


			<div class="col-sm-4">

				<img src="images/logo-pci.png" class="center-block">

				<br>

				<p>PCI’s mission is to empower people to enhance health, end hunger and overcome hardship.</p>

				<br>

				<p>Motivated by their concern for the world's most vulnerable children, families and communities, PCI envisions a world where abundant resources are shared, communities are able to provide for the health and well-being of their members, and children and families can achieve lives of hope, good health and self-sufficiency.</p>

			</div>

		</div>

		<br><br><br><br>

		<div class="row">

			<div class="col-sm-6">
				
				<img src="images/logo-ibridges.png" alt="" class="center-block">

				<br>
								
				<p>The iBRIDGES initiative is an open, inclusive and collaborative community, free from any political or ideological affiliations, that works towards bringing to life a diversity of impactful initiatives, platforms and spaces that all tangibly serve to contribute to the development of the high-tech entrepreneurial sector in Iran.</p>

				<br>

				<p>Dr. Massih Tayebi was privileged to be a speaker at the 2014 iBRIDGES meeting on the topic Transitioning into Productivity. You can see the video <a href="https://www.youtube.com/watch?v=IvYTbdMnUoo" target="_blank">here</a></p>

			</div>

			<div class="col-sm-6">
				
				<img src="images/logo-voices.png" alt="" class="center-block">

				<br>
								
				<p>Voices for Children transforms the lives of abused children by providing them with volunteer Court Appointed Special Advocates (CASAs). We believe that every child deserves a safe and permanent home, and to that end, will review and monitor every court file in the system, provide a CASA to every child in the foster care system who needs one, and advocate to improve the lives of foster children.</p>

			</div>
		</div>

	</div>
</div>