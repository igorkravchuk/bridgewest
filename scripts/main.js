$(function () {

    $('a[data-filter]').bind('click', function () {

        $(this).parents('[data-filter-control]').find('a[data-filter]').removeClass('active');
        $(this).addClass('active');

        var filter = $(this).attr('data-filter').split(' ');
        var $_target = $($(this).attr('data-filter-target'));

        $_target.find('*[data-cat]').hide();

        for (var cat in filter)
        {
            if (filter[cat] == '*')
            {
                $_target.find('*[data-cat]').css('display', '');
            } else
            {
                $_target.find('*[data-cat="' + filter[cat] + '"]').css('display', '');
            }

        }
        return false;
    });

    $('#team a[data-filter]').bind('click', function () {
        if ($('#carousel-team .carousel-indicators li.active').is(":hidden"))
        {
            var index = $('#carousel-team .carousel-indicators').find('li:visible:first').index();
            $('#carousel-team').carousel(index);
        }
    });

    $('form').submit(function () {

        var $_form = $(this);
        var formData = $_form.serialize();

        $.ajax({
            type: 'POST',
            url: $_form.attr('action'),
            data: formData,
            success: function (data) {
                $_form.trigger('reset')
                $_form.find('.form-success').stop(true, true).fadeIn(300).delay(2000).fadeOut(300);
            },
            error: function (xhr, str) {
                $_form.find('.form-error').stop(true, true).fadeIn(300).delay(2000).fadeOut(300);
            }
        });

        return false;
    })

    $('a.page-scroll').bind('click', function (event) {
        event.preventDefault();

        var anchor = $(this).attr('href');
        var $anchor = $(anchor);

        if ($anchor.length)
        {
            $('html, body').stop().animate({
                scrollTop: $anchor.offset().top - 85
            }, 1200, 'easeInOutCubic');
            selectMenu($(this).attr('href'));
        } else
        {
            window.location.href = '/' + anchor;
        }

    });

    function selectMenu(target) {
        $('#mainMenu li').removeClass('active');
        $('#mainMenu li').each(function () {
            var $this = $(this);

            if ($this.find('a').attr('href') === target)
            {
                $this.addClass('active');
            }
        })
    }

    $('.invest-item').hover(function () {
        $(this).addClass('show-detail');
    },
        function () {
            $(this).removeClass('show-detail');
        }).bind('click', function () {
            if (!$(this).hasClass('show-detail'))
            {
                $(this).addClass('show-detail');
            }
        })

    $('.invest-close').bind('click', function (e) {

        $(this).parent().toggleClass('show-detail');
        e.stopPropagation()
    });

    carouselEqualHeight();

    loadLocalNews();
});

$(window).resize(function () {
    carouselEqualHeight();
});

function carouselEqualHeight() {

    var $carouselContainer = $('#carousel-team .carousel-inner');
    var maxHeight = 0;

    $carouselContainer.find('.item').each(function () {
        var $this = $(this);

        $this.css('display', 'block')
        if ($this.height() > maxHeight)
        {
            maxHeight = $this.height();
        }
        $this.removeAttr('style');
    })

    $carouselContainer.height(maxHeight);
}

function sortDescending(a, b) {
    var date1 = $(a).find(".news-item-time").text().trim();	
    date1 = date1.split('.');
    date1 = new Date(parseInt(date1[2]), parseInt(date1[0])-1, parseInt(date1[1]));
	
	
    var date2 = $(b).find(".news-item-time").text().trim();
    date2 = date2.split('.');
    date2 = new Date(parseInt(date2[2]), parseInt(date2[0])-1, parseInt(date2[1]));
    return date1 - date2;
};

function loadLocalNews() {

    $.ajax({
        type: "GET",
        url: "/news.xml",
        dataType: "xml",
        success: function (xml) {
			
            var isHomepage = window.location.pathname === '/';
			console.log(isHomepage);
			var newsItemClass = isHomepage ? 'news-item col-sm-3' : 'news-item';
			
            $(xml).find('release').each(function () {
                var id = $(this).find('id').text();
                var headline = $(this).find('headline').text();
                var modified = $(this).find('modified').text().trim();
                var date = modified.split('/');
				date = new Date(parseInt(date[2]), parseInt(date[0])-1, parseInt(date[1]));

                $(".news-items-container").append('<div class="' + newsItemClass + '">' +
                    '<p class="news-item-time">' + $.format.date(date, 'MM.dd.yyyy') + '</p>' +
                    '<h4 class="news-item-title">' +
                        '<a href="/news/' + id + '">' + headline + '</a>' +
                    '</h4>' +
					(isHomepage ? '<p><a href="/news/' + id + '" class="news-item-readmore">Read more</a></p>' 
					: '') +
                    '</div>');
            });
			var items = $('.news-items-container .news-item').sort(sortDescending);	

			if(items.length)
			{
				items.each(function(index){					 
					$(".news-items-container").prepend(this);	
				});	
				
				var cnt = isHomepage ? 4 : 25;
				
				$(".news-items-container").html($(".news-items-container .news-item").slice(0, cnt));	
			}					
        }
    });
}