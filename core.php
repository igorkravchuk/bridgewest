<?php

function xml2array ($xmlObject, $out = array())
{
    foreach ( (array) $xmlObject as $index => $node )
	{
        $out[$index] = is_object($node) ? xml2array($node) : $node;
	}

    return $out;
}

function getNews()
{
	$url = "http://bridgewestcapital.mediaroom.com/api/newsfeed_releases/list.php";
	$xml = simplexml_load_file($url);
	$releases = xml2array($xml);
	return $releases['release'];
}

function startsWith($haystack, $needle)
{
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

function getNewsDetail($id)
{
	if (startsWith($id, '00') === false) {
		$url = "http://bridgewestcapital.mediaroom.com/api/newsfeed_releases/get.php?id=".$id;	
		$xml = simplexml_load_file($url);
		$releases = xml2array($xml);
		return $releases;		
	} else {
		$url = dirname(__FILE__).'/news.xml';
		$xml = simplexml_load_file($url);		
		
		foreach ($xml->release as $release) {			
			if($release->id == $id){
				
				//echo $release->body->nodeValue;
				//$release->body->nodeValue = '123';
				
				//$xml->saveXML();
				return xml2array($release);		
			}
		}		
	}
}
?>