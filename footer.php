	<div class="section footer-container">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<p>
						<b>San Diego Innovation Center</b>
						<br>
						7310 Miramar Road, Suite 500
						San Diego, CA 92126 
				</div>	

				<div class="col-md-6">
					<ul class="list-unstyled">
						<li><a href="tel:8584122027">(858) 412-2027</a></li>
						<li><a href="mailto:info@bridgewestgroup.com">info@bridgewestgroup.com</a></li>
						<li><a href="mailto:careers@bridgewestgroup.com">careers@bridgewestgroup.com</a></li>
					</ul>
				</div>

				<div class="col-md-3">
					<ul class="list-unstyled">
						<li><a href="#">Bridgewest Group</a></li>
						<li><a href="terms">Terms of Use</a></li>
						<li><a href="privacy">Privacy Policy</a></li>
					</ul>
				</div>
			</div>
		</div>

		<hr>

		<div class="container">
			<p class="text-center text-muted">
				All Rights Reserved &copy; <?php echo date("Y"); ?>
			</p>
		</div>
	</div>




	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-dateFormat/1.0/jquery.dateFormat.min.js"></script>

	<script src="/scripts/main.js"></script>
	
</body>
</html>