
	<div class="container">

		<div id="carousel-home" class="carousel carousel-home slide" data-ride="carousel">

			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#carousel-home" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-home" data-slide-to="1"></li>
				<li data-target="#carousel-home" data-slide-to="2"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">

				<div class="item active" style="background-image: url('images/slide-1.png')">
					<div class="carousel-caption">
						<h2>Long-team investor</h2>
						<p>The Bridgewest Group is an opportunistic investor that creates long-term value through the application of superior industry knowledge, operational expertise and significant financial resources to attractive investment opportunities.</p>
					</div>
				</div>
				<div class="item" style="background-image: url('images/slide-2.png')">
					<div class="carousel-caption">
						<h2>Long-team investor</h2>
						<p>The Bridgewest Group is an opportunistic investor that creates long-term value through the application of superior industry knowledge, operational expertise and significant financial resources to attractive investment opportunities.</p>
					</div>
				</div>
				<div class="item" style="background-image: url('images/slide-3.png')">
					<div class="carousel-caption">
						<h2>Long-team investor</h2>
						<p>The Bridgewest Group is an opportunistic investor that creates long-term value through the application of superior industry knowledge, operational expertise and significant financial resources to attractive investment opportunities.</p>
					</div>
				</div>

			</div>
		</div>

	</div>
