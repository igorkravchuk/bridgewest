<div class="section" id="foundation">
	<div class="container">
		<h2 class="title-section">
			<span>Bridgewest Foundation</span>
		</h2>

		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<p class="text-center">
					The Bridgewest Group actively invests in the global community through donations to charities with a track record for generating superior social return on investment. Below is a selection of the charities that we are proud to support.
				</p>
			</div>
		</div>

		<br><br>
		

		<div class="row">

			<div class="col-sm-4">

				<img src="images/logo-pci.png" class="center-block">

				<br>

				<h4 class="text-uppercase">Mission</h4>
				<p>PCI's mission is to prevent disease, improve community health and promote sustainable development worldwide.</p>

				<br>

				<p>Motivated by their concern for the world's most vulnerable children, families and communities, PCI envisions a world where abundant resources are shared, communities are able to provide for the health and well-being of their members, and children and families can achieve lives of hope, good health and self-sufficiency.</p>

			</div>

			<div class="col-sm-4">
				
				<img src="images/logo-family.png" alt="" class="center-block">

				<br>

				<h4 class="text-uppercase">Mission</h4>
				<p>To support individual and family readiness through an array of programs specifically targeted to assist the Naval Special Warfare community in maintaining a resilient, sustainable, and healthy force in this era of persistent conflict and frequent deployments.</p>

				<br>

				<p>Their vision is to be the premier provider of benevolent civilian support services to the NSW communities they serve. This is pursued by collaborating with the leaders in Family Support to better understand the effects of deployment; by frequent analysis to ensure they are meeting current needs; by the delivery of exceptional programs; and by educating and involving the civilian population in support of these elite warriors.</p>

			</div>

			<div class="col-sm-4">
				
				<img src="images/logo-ibridges.png" alt="" class="center-block">

				<br>
				
				<h4 class="text-uppercase">Mission</h4>
				
				<p>The iBRIDGES initiative is an open, inclusive and collaborative community, free from any political or ideological affiliations, that works towards bringing to life a diversity of impactful initiatives, platforms and spaces that all tangibly serve to contribute to the development of the high-tech entrepreneurial sector in Iran.</p>

				<br>

				<p>Dr. Massih Tayebi was privileged to be a speaker at the 2014 iBRIDGES meeting on the topic Transitioning into Productivity. You can see the video here</p>

			</div>
		</div>

	</div>
</div>